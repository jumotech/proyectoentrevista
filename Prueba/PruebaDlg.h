
// PruebaDlg.h : header file
//

#pragma once
#include "atlimage.h"

// CPruebaDlg dialog
class CPruebaDlg : public CDialogEx
{
// Construction
public:
	CPruebaDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PRUEBA_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnBnClickedBtnOpenImg();
	afx_msg void OnBnClickedBtnShowImg();
	afx_msg void OnBnClickedBtnCalcula();
	/**
	* @brief Shows image fie.
	*/
	void DrawImage();

	/**
	* @brief Make grayscale (slow).
	*/
	//void EscalaGrises_Slow();

	/**
	* @brief Make grayscale (fast).
	*/
	void EscalaGrises();

private:
	CString m_imageFilePath;
	ATL::CImage m_image;
	ATL::CImage m_imageGrayscale;
};