
// PruebaDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Prueba.h"
#include "PruebaDlg.h"
#include "afxdialogex.h"

#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include <stdio.h>

using namespace std;
using namespace cv;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CPruebaDlg dialog

CPruebaDlg::CPruebaDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PRUEBA_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPruebaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPruebaDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_OPEN_IMG, &CPruebaDlg::OnBnClickedBtnOpenImg)
	ON_BN_CLICKED(IDC_BTN_SHOW_IMG, &CPruebaDlg::OnBnClickedBtnShowImg)
	ON_BN_CLICKED(IDC_CALC_MEAN, &CPruebaDlg::OnBnClickedBtnCalcula)
END_MESSAGE_MAP()

// CPruebaDlg message handlers

BOOL CPruebaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPruebaDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPruebaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centra en un rectángulo
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibuja el recuadro
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		DrawImage();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPruebaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPruebaDlg::OnBnClickedBtnOpenImg()
{
	CFileDialog dlg(TRUE,
		_T("jpeg"),
		NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T("JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg|BMP Files (*.bmp)|*.bmp|All Files (*.*)|*.*||"),
		NULL);

	if (dlg.DoModal() == IDOK)
	{
		m_imageFilePath = dlg.GetPathName();
		SetDlgItemText(IDC_EDT_FILEPATH, m_imageFilePath);
	}
}

void CPruebaDlg::OnBnClickedBtnShowImg()
{
	if (!m_image.IsNull())
	{
		m_image.Destroy();
	}

	m_image.Load(m_imageFilePath);
	
	EscalaGrises();

	Invalidate();
}

void CPruebaDlg::OnBnClickedBtnCalcula()
{
	// Calculo de la media
	// Se carga el vector tipo Mat de la librería OpenCV de la imagen demo
	cv::Mat img = cv::imread("res/img.jpeg");
	vector<Mat> channels;
	// Se declara el la instancia channels para almacenar los canales de la imagen
	split(img, channels);
	// Se calcula la media de los píxeles del canal[0]
	Scalar me = mean(channels[0]);
	// Se declara un string para mostrar la salida por el Messagebox
	CString test1;
	test1.Format(_T("La media es: %d"),me);
	MessageBox(NULL, test1);
	
	// Calculo de la mediana del canal 
	cv::Mat channel = channels[0];
    double im = (channel.rows*channel.cols) / 2;
	int bin = 0;
	double med = -1.0;

	int histSize = 256;
	float range[] = { 0, 256 };
	const float* histRange = { range };
	bool uniform = true;
	bool accumulate = false;

	cv::Mat hist;
	cv::calcHist(&channel, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);

	for (int i = 0; i < histSize && med < 0.0; ++i)
	{
		bin += cvRound(hist.at< float >(i));
		if (bin > im && med < 0.0)
			med = i;
	}
	CString test2;
	test2.Format(_T("La mediana es: %d"), med);
	MessageBox(NULL, test2);
}

void CPruebaDlg::DrawImage()
{
	if (m_image.IsNull() || m_imageGrayscale.IsNull())
	{
		return;
	}

	CWnd* pWnd = GetDlgItem(IDC_STC_IMAGE);
	CDC* pDC = pWnd->GetDC();

	pDC->SetStretchBltMode(COLORONCOLOR);

	CRect rect;
	pWnd->GetClientRect(&rect);
	ClientToScreen(&rect);

	int newWidth = (m_image.GetWidth()*rect.Height()) / m_image.GetHeight();
	int newHeight = rect.Height();

	CButton* pChkGrayscale = (CButton*)GetDlgItem(IDC_CHK_GRAYSCALE);
	if (BST_CHECKED == pChkGrayscale->GetCheck())
	{
		m_imageGrayscale.Draw(pDC->GetSafeHdc(), 0, 0, newWidth, newHeight);
	}
	else
	{
		m_image.Draw(pDC->GetSafeHdc(), 0, 0, newWidth, newHeight);
	}

	pWnd->ReleaseDC(pDC);
}


void CPruebaDlg::EscalaGrises()
{
	int	width = m_image.GetWidth();
	int	height = m_image.GetHeight();

	if (!m_imageGrayscale.IsNull())
	{
		m_imageGrayscale.Destroy();
	}
	m_imageGrayscale.Create(width, height, m_image.GetBPP());

	int pitch = m_image.GetPitch();
	BYTE* pInImage = (BYTE*)m_image.GetBits();
	BYTE* pOutImage = (BYTE*)m_imageGrayscale.GetBits();

	long lAdrs;
	double gray;
	BYTE bRed, bGreen, bBlue;

	DWORD start = GetTickCount();
	for (int y = 0; y<height; ++y)
	{
		for (int x = 0; x<width; ++x)
		{
			lAdrs = y * pitch + x * 3;
			bRed = *(pInImage + lAdrs);
			bGreen = *(pInImage + lAdrs + 1);
			bBlue = *(pInImage + lAdrs + 2);

			gray = bRed * 0.587 + bGreen * 0.299 + bBlue * 0.114;
			*(pOutImage + lAdrs) = static_cast<BYTE>(gray);
			*(pOutImage + lAdrs + 1) = static_cast<BYTE>(gray);
			*(pOutImage + lAdrs + 2) = static_cast<BYTE>(gray);
		}
	}
	DWORD end = GetTickCount();
	DWORD time = end - start;
}

